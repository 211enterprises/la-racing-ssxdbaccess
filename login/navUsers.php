<?php 
session_start();
if(empty($_SESSION['userId'])){
	header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
	echo "\$_SESSION['userId'] error";
}
//            if(isset($_COOKIE['remRef']) && isset($_SESSION['userId']) ){header('location: ' . $_COOKIE['remRef'] );}
//            else {setcookie('remRef' , $_GET['ref'], time()+60 ); }
include("../fPDF/config.php");

echo "  <link type=\"text/css\" rel=\"stylesheet\" href=\"navUsers.css\">";
// accessLevel is stored in the regUserTbl table, regAccessLevel field
// it is also to be the column name in the race_pass_types table, where usertype specific pricing is set

$vDate = date( 'Y-m-d' );


if( $_SESSION['accessLevel'] == md5('admin') )
  {
  include $_SERVER['DOCUMENT_ROOT'] . '/login/navAdmin.nav';
  }//if admin


if( $_SESSION['accessLevel'] == md5('admin') || $_SESSION['accessLevel'] == md5('moderator') )
  {
  include $_SERVER['DOCUMENT_ROOT'] . '/login/navModerator.nav';
  }//if admin


if( $_SESSION['accessLevel'] == md5('admin') || $_SESSION['accessLevel'] == md5('user') )
  {
  include $_SERVER['DOCUMENT_ROOT'] . '/login/navUser.nav';
  }//if admin


if( $_SESSION['accessLevel'] == md5('admin') || $_SESSION['accessLevel'] == md5('sales') )
  {
  include $_SERVER['DOCUMENT_ROOT'] . '/login/navSales.nav';
  }//if admin


if( $_SESSION['accessLevel'] == md5('admin') || $_SESSION['accessLevel'] == md5('agent') )
  {
  include $_SERVER['DOCUMENT_ROOT'] . '/login/navAgent.nav';
  }//if admin


if( $_SESSION['accessLevel'] == md5('admin') || $_SESSION['accessLevel'] == md5('barter') )
  {
  include $_SERVER['DOCUMENT_ROOT'] . '/login/navBarter.nav';
  }//if admin


?>