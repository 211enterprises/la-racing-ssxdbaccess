<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LA Racing Office Login</title>
 <link type="text/css" rel="stylesheet" href="login.css">
</head>

<body>
<div id="userNameWrap">
	<form id="loginFrm" name="loginFrm" action="<?php echo $_SERVER['PHP_SELF']; ?>?cmd=lg" method="post">
    <h4><?php echo $errorMess; ?></h4>
	<div class="userNameRow">
    	<span class="userClassLeft">User Name:</span><span class="userClassRight"><input name="userName" type="text" class="<?php echo $aClass; ?>" id="userName" value="<?php echo (!isset($_COOKIE['remMe']) ? $_POST['userName'] : $_COOKIE['remMe']); ?>" size="28" />
    	</span>
    </div>
    <div class="userNameRow">
        <span class="userClassLeft">Password:</span><span class="userClassRight"><input name="passWord" type="password" class="<?php echo $aClass; ?>" id="passWord" value="<?php echo (!isset($_COOKIE['remPass']) ? $_POST['passWord'] : $_COOKIE['remPass']); ?>" size="28" />
        </span>
   	</div>
    <div class="userNameRow">
        <span class="userClassLeft">Remember Me:</span><span class="userClassRight"><input name="remMe" type="checkbox" id="remMe" value="1" checked />
        </span>
   	    <input class="blueButtonBgClass" name="Submit" type="Submit" value="Submit" />
    </div>
    <div class="userNameRow">
    	<div align="center"></div>
    </div>
    </form>
</div>
</body>
</html>
