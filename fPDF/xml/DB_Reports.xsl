<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
http://www.programmersheaven.com/2/FAQ-XML-Display-Nested-XML-XSLT

http://www.bennadel.com/blog/1080-Recursive-XSLT-For-Nested-XML-Nodes-In-ColdFusion.htm
-->

<html xsl:version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <body style="font-family:Verdana,Arial;font-size:12pt;background-color:#EEEEEE">

    <xsl:for-each select="sales_report/period">
      <div style="background-color:teal;color:white;padding:2px">
        <span style="font-weight:bold"><xsl:value-of select="from"/></span> - <xsl:value-of select="to"/>
      </div>
    </xsl:for-each>
    <xsl:for-each select="sales_report/school">
      <div style="margin-left:20px;margin-bottom:1em;font-size:10pt">
        <xsl:value-of select="school_name"/>
      </div>
    </xsl:for-each>
      <xsl:for-each select="sales_report/school/school_totals">
        <div style="margin-left:20px;margin-bottom:1em;font-size:10pt">
          <xsl:value-of select="school_contribution"/>
          <span style="font-style:italic"> Total School Passes - <xsl:value-of select="school_all_passes"/>  --  </span>
          <span style="font-style:italic;color:#ff0000"> [<xsl:value-of select="school_adv10"/> - Adv10]   </span>
          <span style="font-style:italic;color:#fff000"> [<xsl:value-of select="school_adv20"/> - Adv20]   </span>
          <span style="font-style:italic;color:#30cd00"> [<xsl:value-of select="school_adv40"/> - Adv40]   </span>
        </div>
      </xsl:for-each>


      <xsl:for-each select="sales_report/school/team">
        <div style="margin-left:20px;margin-bottom:1em;font-size:10pt">
              <xsl:value-of select="team_name"/> -- 
        </div>
      </xsl:for-each>


        <xsl:for-each select="sales_report/school/team/team_totals">
            <div style="margin-left:20px;margin-bottom:1em;font-size:10pt">
              <xsl:value-of select="team_contribution"/>
              <span style="font-style:italic"> Total Team's Passes - <xsl:value-of select="team_all_passes"/>  </span>
              <span style="font-style:italic;color:#ff0000"> [<xsl:value-of select="team_adv10"/> - Adv10]   </span>
              <span style="font-style:italic;color:#fff000"> [<xsl:value-of select="team_adv20"/> - Adv20]   </span>
              <span style="font-style:italic;color:#30cd00"> [<xsl:value-of select="team_adv40"/> - Adv40]   </span>
            </div>
          </xsl:for-each>



          <xsl:for-each select="sales_report/school/team/student">
            <div style="margin-left:20px;margin-bottom:1em;font-size:10pt">
              <xsl:value-of select="student_name"/> -- 
            </div>
          </xsl:for-each>
                <xsl:for-each select="sales_report/school/team/student/student_totals">
                <div style="margin-left:20px;margin-bottom:1em;font-size:10pt">
                  <xsl:value-of select="student_contribution"/> -- 
                  <span style="font-style:italic"> Total of Student's Passes - <xsl:value-of select="student_all_passes"/>  </span>
                  <span style="font-style:italic;color:#ff0000"> [<xsl:value-of select="student_adv40"/> - Adv10]   </span>
                  <span style="font-style:italic;color:#fff000"> [<xsl:value-of select="student_adv40"/> - Adv20]   </span>
                  <span style="font-style:italic;color:#30cd00"> [<xsl:value-of select="student_adv40"/> - Adv40]   </span>
                </div>
                </xsl:for-each>
                <xsl:for-each select="sales_report/school/team/student/pass_details">
                  <div style="margin-left:20px;margin-bottom:1em;font-size:10pt">
                    <xsl:value-of select="date"/> -- 
                    <span style="font-style:italic"> Order# <xsl:value-of select="receipt_num"/>  -- </span>
                    <span style="font-style:italic"> Qty <xsl:value-of select="qty"/>   -- </span>
                    <span style="font-style:italic"> <xsl:value-of select="contribution"/> Contribution   </span>
                  </div>
                </xsl:for-each>
  </body>
</html>
