<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2009-10-14  -  class_sched_d.php
#  
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
session_start();
if(empty($_SESSION['userId'])){
	header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
	echo "\$_SESSION['userId'] error";
}
//echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";


require_once('fpdf.php'); 
require_once('fpdi.php'); 

include("config.php");
include("ssxDBfunctions.php");

/*
      echo "\n<form style=\"display:inline;margin:0;padding:0;\" action=\"class_sched_d.php\" target=\"class_sched_d\" method=\"post\">\n";
      echo " <input type=\"hidden\" value=\"".$_POST[class_date]."\" name=\"class_date\" />\n";
      echo " <input type=\"hidden\" value=\"".$_POST[class_time]."\" name=\"class_time\" />\n";
      echo "<input style=\"display:inline;\" class=\"submitButton\" name=\"submit\" type=\"submit\" value=\"Print for Pit Road\" />\n";
      echo "</form>\n";
*/


$pdf =& new FPDI(); 
$class_roster_template = 'blanks/'.$_POST[location].'/class_roster_larx.pdf';

$pagecount = $pdf->setSourceFile( $class_roster_template ); 
$tplidx = $pdf->importPage(1, '/MediaBox'); 
 
$pdf->addPage(); 
$pdf->useTemplate($tplidx, 2, 1, 214); 
$pdf->AddFont('BankGothicBT-Medium','','bankgothicmedium.php');
$pdf->SetFont('BankGothicBT-Medium','',14);
$pdf->SetAutoPageBreak( false );

     $pdf->SetXY( 70 , 21 );
     $pdf->Cell( 78 , 5 , $_POST[class_date].' @ '.$_POST[class_time], 0, 0, 'C', false);


$con = mysql_connect($DBhost,$DBuser,$DBpass) or die("Unable to connect to database $DBName");
$db  = mysql_select_db($DBName,$con) or die("Unable to select database $DBName");

$class_date=0; if( $_GET[class_date] ){ $class_date=$_GET[class_date]; } if( $_POST[class_date] ){ $class_date=$_POST[class_date]; } // POST takes priority!?!
$class_time=0; if( $_GET[class_time] ){ $class_time=$_GET[class_time]; } if( $_POST[class_time] ){ $class_time=$_POST[class_time]; } // POST takes priority!?!
$location=0; if( $_GET[location] ){ $location=$_GET[location]; } if( $_POST[location] ){ $location=$_POST[location]; } // POST takes priority!?!
$form_vertical_output = 45;
$form_vertical_spacing = 4.65;

$class_day_order_sql = "select class_day_order from race_passes where location = '".$location."' and class_date = '".$class_date."' and class_time = '".$class_time."' group by class_day_order ";
$class_day_order_query=mysql_query( $class_day_order_sql, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$class_day_order_groups = fetch_all( $class_day_order_query );

$pdf->SetFont('BankGothicBT-Medium','',8);
$it = 0; // probably should have just converted to a for loop...
         // while loop worksed fine until we got non-sequential group numbers then stopped pulling properly
         // so simply added a sequential iterator thru the main array to pull the data out of it...not 100%
         // why it would not pull that data from the whiled sub-arrays
 while($class_day_order_group = each($class_day_order_groups) ) 
  {
  $race_passes_sql = "select * from race_passes where location = '".$location."' and class_date = '".$class_date."' and class_time = '".$class_time."' and class_day_order = '".$class_day_order_groups[$it][0]."'  ";
  $it++;
  $race_passes_query=mysql_query( $race_passes_sql, $con )  or die('Could not Pull From DB:: ' . mysql_error());
  $race_passes_rows = fetch_all( $race_passes_query );
  //$num_rows = mysql_num_rows($race_passes_query);
    while($row = each($race_passes_rows) ) 
     {
     if( $row[1][video_paid] == '0.00' ){ $row[1][video_paid] = '-'; }
     if( $row[1][insurance_paid] == '0.00' ){ $row[1][insurance_paid] = '-'; }

     $pdf->SetXY( 7 , $form_vertical_output );
     $pdf->Cell(55 , 5 , $row[1][id].' - '.$row[1][driver_name], 0, 0, 'R', false);
     $pdf->SetXY( 63 , $form_vertical_output );
     $pdf->Cell(24 , 5 , $row[1][race_pass_type], 0, 0, 'C', false);
     $pdf->SetXY( 87 , $form_vertical_output );
     $pdf->Cell(15 , 5 , $row[1][class_day_order], 0, 0, 'C', false);
     $pdf->SetXY( 103 , $form_vertical_output );
     $pdf->Cell(17 , 5 , $row[1][video_paid], 0, 0, 'C', false);
     $pdf->SetXY( 121 , $form_vertical_output );
     $pdf->Cell(17 , 5 , $row[1][insurance_paid], 0, 0, 'C', false);
     $pdf->SetXY( 158 , $form_vertical_output );
     $pdf->Cell(53 , 5 , $row[1][class_day_notes], 0, 0, 'C', false);
     
     $form_vertical_output += $form_vertical_spacing;
     }  //end while 'race_passes_rows' rows
  $form_vertical_output += $form_vertical_spacing;
  $form_vertical_output += $form_vertical_spacing;
  }  //end while 'class_day_order_groups' rows


$file_name = "LARX_class_roster_".$_POST[location]."_".$_POST[class_date]."_".$_POST[class_time]."_[".time()."].pdf";
	 
$pdf->Output("class_rosters/".$file_name, 'F'); //saves as a file on the server
$pdf->Output($file_name, 'D'); //forces a download in the browser

mysql_close( $con );


?> 