<?PHP

function confirm_space_remains( $date_from,$date_to,$time_from,$time_to,$location,$override,$con ) 
  {
$now  = date( 'Y-m-d H:i:s' );
$year = date('Y');
$month= date('m');
$day  = date('d');

// $override == 'disturbence_in_the_force'

  if( (( $date_to == '0000-00-00' || $date_to == '9999-12-31' || $date_to == $year.'-'.$month.'-'.$day ) && $time_to == '00:00:00' ) || $override == 'disturbence_in_the_force' )
     {
     return array( $date_from,$date_to,$time_from,$time_to,$target_full );
     }
/*
  $race_passes_sql  = "select * from race_passes where class_date = '".$date_to."' AND class_time = '".$time_to."' AND location = '".$location."' ";
  $race_passes_query= mysql_query( $race_passes_sql, $con )  or die('SchedLockout Could not Pull From race_passes DB table:: ' . mysql_error());
  $race_passes_rows = fetch_all( $race_passes_query );

  echo "\n\n\n<pre>race_passes_rows\n===============================================================\n\n\n";
  echo $race_passes_sql;
  print_r( $race_passes_rows );
  echo "\n\n\n===============================================================</pre>\n\n\n";
*/

  $capacity_sql  = "SELECT class_date, class_time, location, race_pass_type, race_pass_laps, COUNT( * ) AS cars FROM race_passes WHERE class_date = '".$date_to."' AND class_time = '".$time_to."' AND location = '".$location."' GROUP BY class_date,location,class_time,race_pass_laps ";
  $capacity_query= mysql_query( $capacity_sql, $con )  or die('SchedLockout Could not Pull From training_dates DB table:: ' . mysql_error());
  $capacity_rows = fetch_all( $capacity_query );

/*
  echo "\n\n\n<pre>capacity_rows\n===============================================================\n\n\n";
  echo $capacity_sql;
  print_r( $capacity_rows );
  echo "\n\n\n===============================================================</pre>\n\n\n";
*/

  $training_dates_sql  = "select * from training_dates where date = '".$date_to."' AND time = '".$time_to."' AND location = '".$location."' order by date asc ";
  $training_dates_query= mysql_query( $training_dates_sql, $con )  or die('SchedLockout Could not Pull From training_dates DB table:: ' . mysql_error());
  $training_dates_rows = fetch_all( $training_dates_query );

/*
  echo "\n\n\n<pre>training_dates_rows\n===============================================================\n\n\n";
  echo $training_dates_sql;
  print_r( $training_dates_rows );
  echo "\n\n\n===============================================================</pre>\n\n\n";
*/


$avail_date       = $training_dates_rows[0][date];
$avail_time       = $training_dates_rows[0][time];
$ave_lap_time     = $training_dates_rows[0][ave_lap_time];
$max_driving_time = $training_dates_rows[0][driving_time];
$cars_per_session = $training_dates_rows[0][cars_per_session];
$driving_time     = 0;
$target_full      = "";

  while( count( $capacity_rows ) > 0 && $row = each($capacity_rows ) ) 
     {
       {
       $cars = $row[1][cars] + 1;
       $sessions = ceil( $cars / $cars_per_session );
       $driving_time += ($row[1][race_pass_laps] * $ave_lap_time * $sessions);
  echo "\n\ncars = ".$cars."\ncars_per_session = ".$cars_per_session."\nrow[1][race_pass_laps] = ".$row[1][race_pass_laps]."\nave_lap_time = ".$ave_lap_time."\nsessions = ".$sessions."\n\n";
       }
     }  //end while 'capacity' rows
  if( $driving_time > $max_driving_time )
     {
     $target_full = "\nThe selected class is full and cannot be added to --- ".$date_to. " @ ".$time_to.". Remaining scheduled for ".$date_from." @ ".$time_from."\n";
     $date_to = $date_from;
     $time_to = $time_from;
     }
  elseif( ( $avail_date != $date_to) || ( $avail_time != $time_to) )
     {
     $target_full = "\nThe selected class is NOT AVAILABLE and cannot be added to --- ".$date_to. " @ ".$time_to.". Remaining scheduled for ".$date_from." @ ".$time_from."\n";
     $date_to = $date_from;
     $time_to = $time_from;
     }
  return array( $date_from,$date_to,$time_from,$time_to,$target_full );
  }



/*
capacity_rows
===============================================================


SELECT class_date, class_time, location, race_pass_type, race_pass_laps, COUNT( * ) FROM race_passes WHERE class_date = '2012-04-29' AND class_time = '08:00:00' AND location = 'LosAngeles' GROUP BY race_pass_laps,class_date,location,class_time Array
(
    [0] => Array
        (
            [0] => 2012-04-29
            [class_date] => 2012-04-29
            [1] => 08:00:00
            [class_time] => 08:00:00
            [2] => LosAngeles
            [location] => LosAngeles
            [3] => SuperRide
            [race_pass_type] => SuperRide
            [4] => 0
            [race_pass_laps] => 0
            [5] => 2
            [COUNT( * )] => 2
        )

    [1] => Array
        (
            [0] => 2012-04-29
            [class_date] => 2012-04-29
            [1] => 08:00:00
            [class_time] => 08:00:00
            [2] => LosAngeles
            [location] => LosAngeles
            [3] => Adventure 20
            [race_pass_type] => Adventure 20
            [4] => 20
            [race_pass_laps] => 20
            [5] => 24
            [COUNT( * )] => 24
        )

)



===============================================================

training_dates_rows
===============================================================


select * from training_dates where date = '2012-04-29' AND time = '08:00:00' AND location = 'LosAngeles' order by date asc Array
(
    [0] => Array
        (
            [0] => 675
            [id] => 675
            [1] => [L.A.] - Stock Car Adventures - Adv40 & Adv20
            [class_type] => [L.A.] - Stock Car Adventures - Adv40 & Adv20
            [2] => 2012-04-29
            [date] => 2012-04-29
            [3] => 08:00:00
            [time] => 08:00:00
            [4] => false
            [is_private] => false
            [5] => full
            [class_status] => full
            [6] => 0
            [student_tally] => 0
            [7] => 0.00
            [rerace_total] => 0.00
            [8] => 0.00
            [insurance_total] => 0.00
            [9] => 0.00
            [video_total] => 0.00
            [10] => pending
            [class_disposition] => pending
            [11] => 0.00
            [track_rental_cost] => 0.00
            [12] => 0.00
            [insurance_cost] => 0.00
            [13] => 0.00
            [fuel_cost] => 0.00
            [14] => 0.00
            [day_labor_cost] => 0.00
            [15] => 0.00
            [other_costs] => 0.00
            [16] => verbally confirmed by BK per Jim - 2011-11-14
made available 2011-11-19
            [notes] => verbally confirmed by BK per Jim - 2011-11-14
made available 2011-11-19
            [17] => LosAngeles
            [location] => LosAngeles
            [18] => 30.00
            [ave_lap_time] => 30.00
            [19] => 3600.00
            [driving_time] => 3600.00
            [20] => 4
            [cars_per_session] => 4
        )

)



===============================================================

*/







?>