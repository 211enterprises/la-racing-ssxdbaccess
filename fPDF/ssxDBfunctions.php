<?PHP

$now  = date( 'Y-m-d H:i:s' );
$year = date('Y');
$month= date('m');
$day  = date('d');

function fetch_all($result) 
  {
  while($row=mysql_fetch_array($result)) 
    {
    $return[] = $row;
    }
  return $return;
  }

function parse_date_string( $format, $mals_date_string )
  {
  $date_parts = preg_split  ( '/[ :]+/' , $mals_date_string );
  if( $date_parts[0]*1 < 10 ){ $date_parts[0] = '0'.$date_parts[0]; }
  $months = Array( 'Zero','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec' );
  $mon = array_search( $date_parts[1], $months ); if( $mon < 10 ){ $mon = '0'.$mon; }
  $expire =intval($date_parts[2])+1;
  if( $format == "date" ){ return $date_parts[2]."-".$mon."-".$date_parts[0]; }
  elseif( $format == "expires" ){ return $expire."-".$mon."-".$date_parts[0]; }
  elseif( $format == "time" ){ return $date_parts[4].":".$date_parts[5].":00"; }
  else{ return $date_parts[2]."-".$mon."-".$date_parts[0]." ".$date_parts[4].":".$date_parts[5].":00"; }
  }

function enum_select($table,$field) 
  {
  $result=mysql_query("SHOW COLUMNS FROM `$table` LIKE '$field'");
  if(mysql_num_rows($result)>0)
    {
    $row=mysql_fetch_row($result);
    $options=explode("','", preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $row[1]));
    $options2 = array();
    foreach ($options as $value) 
      {
      $options2[] = array( 'value' => $value, 'display' => htmlentities($value) );
      }
//    print_r($options);
    } 
  else { $options=array(); }
  return $options;
  } 

function get_selector($array,$src,$name,$index,$default)
  {
  $selector = " <select name=\"".$name."\">\n";
  if( $default ){ $selector .= "  <option selected=\"selected\" value=\"".$default."\"> ".$default." </option>\n"; }
  for($i = 0; $i < sizeof($array); $i++ )
     {
     if( $src == 'enum' ){ $selector .= "  <option  value=\"".$array[$i]."\">".$array[$i]."</option>\n"; }
     elseif( $src == 'table' ){ $selector .= "  <option  value=\"".$array[$i][$index]."\">".$array[$i][$index]."</option>\n"; }
     }
  $selector .= "</select>\n";
  return $selector;
  }


function color_table_style()
  {
  $unassigned = 'ff0000';
  $la         = 'ffdddd';
  $nash       = '9999ff';
  $pink_table_style  ="<style>\n";
  $pink_table_style .="table {background-color:#000000;border:1px solid #ff0000;text-align:center;font-size:80%; font-family: Arial, Helvetica, Verdana;}";
  $pink_table_style .="tr th    {font-size:120%;background-color:#ff0000;padding:0 5px 0 5px;border-right:1px solid #000000;border-left:1px solid #000000;border-top:1px solid #ff0000;border-bottom:1px solid #ff0000;}";
  $pink_table_style .="tr td    {background-color:#".$unassigned.";padding:0 5px 0 5px;border-right:1px solid #000000;border-left:1px solid #000000;border-top:1px solid #ff0000;border-bottom:1px solid #ff0000;}";
  $pink_table_style .="tr.LosAngeles th    {font-size:120%;background-color:#".$la.";padding:0 5px 0 5px;border-right:1px solid #000000;border-left:1px solid #000000;border-top:1px solid #ff0000;border-bottom:1px solid #ff0000;}";
  $pink_table_style .="tr.LosAngeles td    {background-color:#".$la.";padding:0 5px 0 5px;border-right:1px solid #000000;border-left:1px solid #000000;border-top:1px solid #ff0000;border-bottom:1px solid #ff0000;}";
  $pink_table_style .="tr.Nashville th     {font-size:120%;background-color:#".$nash.";padding:0 5px 0 5px;border-right:1px solid #000000;border-left:1px solid #000000;border-top:1px solid #ff0000;border-bottom:1px solid #ff0000;}";
  $pink_table_style .="tr.Nashville td     {background-color:#".$nash.";padding:0 5px 0 5px;border-right:1px solid #000000;border-left:1px solid #000000;border-top:1px solid #ff0000;border-bottom:1px solid #ff0000;}";
  $pink_table_style .="</style>\n</head>\n<body>";
  return $pink_table_style;
  }



/*
if($_POST or ($_GET and (!isset($_SESSION['skipnextget']))))      //is there POST or GET data we should look at
  {
  if($_GET)  //is there GET data?
    {
    $newgetarray = array();
    foreach($_GET as $key => $value)
      {
      if(is_array($value)) { $newvalue = implode(',',$value);  }  //if it's an array, convert it to comma separated
      else { $newvalue = $value; }
      $newgetarray[] = $key.'='.$newvalue;
      }
    $newget = implode('&',$newgetarray);
    $_SESSION['skipnextget'] = 1;
    }
  foreach($_POST as $key => $value) { $this[$key] = $value; }
  $_SESSION['postvars'] = serialize($this); //put POST in the session
  header("Location: http://" .$_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?' . $newget); //reload page without POST data and shorter GET data
  exit();
  }
  else   //after reloading we'll come right to this spot
  {
  session_unregister('skipnextget');
  if(isset($_SESSION['postvars']))
    {
    $this = unserialize($_SESSION['postvars']); //load the POST variables from the session
    session_unregister('postvars'); //delete the session stuff
    return $this;
    }
  }

*/











?>