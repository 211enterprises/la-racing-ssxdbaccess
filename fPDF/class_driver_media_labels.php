<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2012-07-14  -  class_driver_media_labels.php
#  
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
session_start();
if(empty($_SESSION['userId'])){
	header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
	echo "\$_SESSION['userId'] error";
}
//echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";


require_once('fpdf.php'); 
require_once('fpdi.php'); 

include("config.php");
include("ssxDBfunctions.php");

/*
      echo "\n<form style=\"display:inline;margin:0;padding:0;\" action=\"class_sched_d.php\" target=\"class_sched_d\" method=\"post\">\n";
      echo " <input type=\"hidden\" value=\"".$_POST[class_date]."\" name=\"class_date\" />\n";
      echo " <input type=\"hidden\" value=\"".$_POST[class_time]."\" name=\"class_time\" />\n";
      echo "<input style=\"display:inline;\" class=\"submitButton\" name=\"submit\" type=\"submit\" value=\"Print for Pit Road\" />\n";
      echo "</form>\n";
*/


$pdf =& new FPDI(); 
$class_roster_template = 'blanks/class_driver_media_labels.pdf';

$pagecount = $pdf->setSourceFile( $class_roster_template ); 
$tplidx = $pdf->importPage(1, '/MediaBox'); 
 
$pdf->addPage(); 
$pdf->useTemplate($tplidx, 0, 0, 0); 
$pdf->AddFont('BankGothicBT-Medium','','bankgothicmedium.php');
$pdf->SetFont('BankGothicBT-Medium','',14);
$pdf->SetAutoPageBreak( false );



$con = mysql_connect($DBhost,$DBuser,$DBpass) or die("Unable to connect to database $DBName");
$db  = mysql_select_db($DBName,$con) or die("Unable to select database $DBName");

$class_date=0; if( $_GET[class_date] ){ $class_date=$_GET[class_date]; } if( $_POST[class_date] ){ $class_date=$_POST[class_date]; } // POST takes priority!?!
$class_time=0; if( $_GET[class_time] ){ $class_time=$_GET[class_time]; } if( $_POST[class_time] ){ $class_time=$_POST[class_time]; } // POST takes priority!?!
$location=0; if( $_GET[location] ){ $location=$_GET[location]; } if( $_POST[location] ){ $location=$_POST[location]; } // POST takes priority!?!
$form_vertical_output = 2;
$form_vertical_spacing = 12.7;
$form_horizontal_output = 5;
$form_horizontal_spacing = 70;

$class_day_roster_sql = "select * from race_passes where location = '".$location."' and class_date = '".$class_date."' and class_time = '".$class_time."' order by class_day_order, id ";
$class_day_roster_query=mysql_query( $class_day_roster_sql, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$class_day_roster_rows = fetch_all( $class_day_roster_query );
$labels_output = 1;
$cols_output = 0;
$count = 1;

   while($row = each($class_day_roster_rows) ) 
     {
     $vert_loc  = $form_vertical_output + ( $form_vertical_spacing * ( $labels_output % 11 ));
     $horiz_loc = $form_horizontal_output + $form_horizontal_spacing * $cols_output;

     $pdf->SetFont('BankGothicBT-Medium','',16);
     $pdf->SetXY( $horiz_loc , $vert_loc );
     $pdf->MultiCell(67 , 10 , ucwords( strtoupper( $row[1][driver_name] ) ), 0, 'L', false);

     $pdf->SetFont('BankGothicBT-Medium','',8);
     $pdf->SetXY( $horiz_loc + 5 , $vert_loc -2 );
     $pdf->Cell(60 , 5 , 'r/p# '.$row[1][id], 0, 0, 'R', false);
     $count++;
     
     $form_vertical_output += $form_vertical_spacing;
     if( $count > 1 && $count % 10 == 1 )
       {
       $labels_output -= ( 20 - $cols_output * 10 );
       $cols_output++;
       }
     $labels_output++;
     }  //end while 'class_day_roster_rows' rows
  $form_vertical_output += $form_vertical_spacing;
  $form_vertical_output += $form_vertical_spacing;


$file_name = "LARX_class_driver_media_labels_".$_POST[location]."_".$_POST[class_date]."_".$_POST[class_time]."_[".time()."].pdf";
	 
$pdf->Output("class_driver_media_labels/".$file_name, 'F'); //saves as a file on the server
$pdf->Output($file_name, 'D'); //forces a download in the browser

mysql_close( $con );


?> 


































