<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2009-10-14  -  Order Confirmation Page part 'B'  -  order_confirm_b.php
#  Part 'B' is where all the "hard work" lives for both the server side scripting and the user.
#  For single item orders, the order data is pulled out of the table and parsed. cart data is 
#  deconstructed to determine Program Type, price, and possible pre-determined dates. Many data
#  fields are preset based on this process. The DB is also queried several times to get the data
#  presets for specific fields to minimize user error entries. Assuming the order was placed 
#  "PROPERLY" to begin with there will be very few entries to be made/changed on this page! but
#  we all know about assuming. Remember the goal here was speed things along by reducing data
#  entry redundancies. Enter it right the first time and there is only minimal SPECIFIC info
#  that needs to be entered along the way, i.e. HERE.
#  
#  A key component in user-ease-of-operations here is the addition of a "previous race pass" 
#  field. this was intended to be the old race pass # that was upgraded for WHATEVER reason, and
#  a pre-defined list is provided. clicking the submit button sends all the data off to part 'C'
#  of the process to commit it to the DB. IF a # was provided the old pass is also VOIDED out and
#  notes added to the record provided. At some point a confirmation process may be necessary, 
#  but has not yet been deemed so.
#  
#  On the quick order page, multiple quantities of multiple items can be ordered in one quick 
#  process. This page does all of the above for ALL items in a given order record. If you have 
#  12 race passes on an order, a race pass details box is created for each. A little redundant 
#  perhaps if all the details are to remain the same for each, but it gives the opportunity to
#  customize each at/near the beginning of the process, rather than trying to go back and re-edit
#  or confirm individual details later. Customers being customers we will inevitably have to do 
#  that, can't simply program customer behaviors away. Plans Change, and the so too the details 
#  on a race pass.
#  
#  Assuming [again] that all data provided is valid for its given field, A RACE PASS is created 
#  for EACH click of the provided submit button. And in complex scenarios, say 3 each of 4 kinds
#  of passes, the user WILL NEED TO BE AWARE of the status of the process to ensure that the 
#  # of the proper type of passes are created. The alternative??? Program away the flexibility
#  that makes 
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
session_start();
if(empty($_SESSION['userId'])){
	header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
	echo "\$_SESSION['userId'] error";
}
echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";



include("config.php");
include("ssxDBfunctions.php");

$con = mysql_connect($DBhost,$DBuser,$DBpass) or die("Unable to connect to database $DBName");
$db  = mysql_select_db($DBName,$con) or die("Unable to select database $DBName");
$sql            ="select *                   from Admin           where       AdminName='$_POST[username]' and AdminPassword='$_POST[password]' ";
$sql_query = "select * from orders where id = '".$_POST[confirm_this_record]."' ";
$query=mysql_query( $sql_query, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$rows = fetch_all( $query );
  $sql_query = "select * from gift_types where 1 ";
  $query=mysql_query( $sql_query, $con )  or die('Could not Pull From DB:: ' . mysql_error());
  $gifts = fetch_all( $query );

$statii = enum_select ( 'race_passes','status' );
$race_pass_mailed = enum_select ( 'race_passes','race_pass_mailed' );
$race_pass_upgrade_type = enum_select ( 'race_passes','race_pass_upgrade_type' );

mysql_close( $con );
   


$status_selector_sched = get_selector( $statii, 'enum', 'status', 0, 'scheduled' );
$status_selector_unsched = get_selector( $statii, 'enum', 'status', 0, 'unscheduled' );
$race_pass_upgrade_type_selector = get_selector( $race_pass_upgrade_type, 'enum', 'race_pass_upgrade_type', 0, 0 );
$race_pass_mailed_selector = get_selector( $race_pass_mailed, 'enum', 'race_pass_mailed', 0, 0 );
$gifts_selector = get_selector( $gifts, 'table', 'gift_type', 1, 0 );

$now = date( 'Y-m-d H:i:s' );



 while($row = each($rows) ) 
  {

  for ($i = 0; $i < count($row); $i++)  { $row[1][$i]=trim( $row[1][$i] ); }   //help to reduce errors on PDF creation in later processing stages, and to just damn well store it as listed!
  echo "For Multi-Pass Orders, click \"Confirm...\" Qty # of times OR for each block!<br/>\n<br/>\n";
  echo $row[1][0]." -- ".$row[1][3]." -- ".$row[1][5]." -- ".$row[1][6]." -- ".$row[1][18]." -- ".$row[1][19]."<br/>\n".$row[1][8]."<br/>\n<br/>\n";
  $cart_items = preg_split  ( '/~/' , $row[1][8] );
      if( $row[1][26] == '' ){ $row[1][26] = $row[1][19]; }
      if( $row[1][27] == '' ){ $row[1][27] = $row[1][18]; }
      if( $row[1][28] == '' ){ $row[1][28] = $row[1][20]; }
      if( $row[1][29] == '' ){ $row[1][29] = $row[1][21]; }
      if( $row[1][30] == '' ){ $row[1][30] = $row[1][22]; }
      if( $row[1][31] == '' ){ $row[1][31] = $row[1][23]; }
   
  while( $cart_item = each( $cart_items ) ) 
    {
    $cart_qtys = preg_split  ( '/:/' , $cart_item[1] );
    for ($i = 0; $i < count($cart_qtys); $i++)  { $cart_qtys[$i]=trim( $cart_qtys[$i] ); }//  //help to reduce errors on PDF creation in later processing stages, and to just damn well store it as listed!


    for ($i = 1; $i <= $cart_qtys[2]; $i++) 
      {
//print_r($cart_qtys);
//      echo $cart_item[1].' ---- '.$i."<br/>\n<br/>\n";
      echo "<div style=\"width:700px;padding-left:10px;padding-right:10px;background-color:#ffdddd;\">\n";
      echo "<br/>\n<form action=\"order_confirm_c.php\" target=\"new\" method=\"post\">\n";
      echo " <input type=\"hidden\" value=\"".$row[1][0]."\" name=\"confirm_this_record\" />\n";
      echo "<input class=\"submitButton\" name=\"submit\" type=\"submit\" value=\"Confirm...\" />\n";
      echo " <input type=\"hidden\" value=\"".$now."\" name=\"created\" />\n";
      if (preg_match("/tbd/i", $cart_qtys[1] )) { $cart_qtys[1] = "0000-00-00"; }
      echo " class_date - <input type=\"text\" value=\"".$cart_qtys[1]."\" name=\"class_date\" />\n";
        echo " class_time - <select name=\"class_time\">\n  <option  value=\"00:00:00\">00:00:00</option>\n  <option  value=\"07:00:00\">07:00:00</option>\n  <option value=\"0730\">07:30:00</option>\n  <option  value=\"08:00:00\">08:00:00</option>\n  <option  value=\"09:00:00\">09:00:00</option>\n  <option  value=\"10:00:00\">10:00:00</option>\n  <option  value=\"11:00:00\">11:00:00</option>\n  <option  value=\"12:00:00\">12:00:00</option>\n  <option  value=\"13:00:00\">13:00:00 - 1pm</option>\n  <option  value=\"14:00:00\">14:00:00 - 2pm</option>\n  <option  value=\"15:00:00\">15:00:00 - 3pm</option>\n  <option  value=\"16:00:00\">16:00:00 - 4pm</option>\n  <option  value=\"17:00:00\">17:00:00 - 5pm</option>\n  <option  value=\"18:00:00\">18:00:00 - 6pm</option>\n  <option  value=\"19:00:00\">19:00:00 - 7pm</option>\n</select>\n\n";
      if (preg_match("/tbd/i", $cart_item[1] )) { echo $status_selector_unsched."\n"; } else { echo $status_selector_sched."\n"; }
      echo "<hr />";
      echo " expires - <input type=\"text\" value=\"".parse_date_string( "expires", $row[1][5] )."\" name=\"expires\" />\n";
      echo " race_pass_group - <input type=\"text\" value=\"".$row[1][3]."\" name=\"race_pass_group\" />\n";
      echo "<hr />";
      echo " name - <input type=\"text\" value=\"".$row[1][26]."\" name=\"driver_name\" />\n";
      echo " company - <input type=\"text\" value=\"".$row[1][27]."\" name=\"driver_company\" />\n";
      echo "<br />";
      echo " addr - <input type=\"text\" value=\"".$row[1][28]."\" name=\"driver_addr1\" />\n";
      echo " city - <input type=\"text\" value=\"".$row[1][29]."\" name=\"driver_addr2\" />\n";
      echo " st - <input size=\"3\" type=\"text\" value=\"".$row[1][30]."\" name=\"driver_state\" />\n";
      echo " zip - <input size=\"5\" type=\"text\" value=\"".$row[1][31]."\" name=\"driver_zip\" />\n";
      echo " cntry - <input size=\"5\" type=\"text\" value=\"USA\" name=\"driver_country\" />\n";
      echo "<hr />";
      echo " email1 - <input type=\"text\" value=\"".$row[1][36]."\" name=\"driver_email1\" />\n";
      echo " email2 - <input type=\"text\" value=\"".$row[1][36]."\" name=\"driver_email2\" />\n";
      echo "<hr />";
      echo " buyer_1st_ph - <input type=\"text\" value=\"".$row[1][34]."\" name=\"buyer_1st_ph\" />\n";
      echo " buyer_2nd_ph - <input type=\"text\" value=\"".$row[1][33]."\" name=\"buyer_2nd_ph\" />\n";
      echo "<br />";
      echo " driver_1st_ph - <input type=\"text\" value=\"".$row[1][33]."\" name=\"driver_1st_ph\" />\n";
      echo " driver_2nd_ph - <input type=\"text\" value=\"".$row[1][34]."\" name=\"driver_2nd_ph\" />\n";
      echo "<br />";
      echo " driver_3rd_ph - <input type=\"text\" value=\"".$row[1][34]."\" name=\"driver_3rd_ph\" />\n";
      echo " driver_fax - <input type=\"text\" value=\"".$row[1][35]."\" name=\"driver_fax\" />\n";
      echo "<hr />";
      echo $gifts_selector."\n";
      echo " gift_from - <input type=\"text\" value=\"Not a Gift\" name=\"gift_from\" />\n";
      echo " do_not_contact_before - <input type=\"text\" value=\"".parse_date_string( "date", $row[1][5] )."\" name=\"do_not_contact_before\" />\n";
      echo "<hr />";
      echo " race_pass_details...<br />\n";
      echo " type - <input type=\"text\" value=\"".$cart_qtys[5]."\" name=\"race_pass_type\" />\n";
      echo " date_sent - <input type=\"text\" value=\"".parse_date_string( "date", $row[1][5] )."\" name=\"race_pass_mailed_date\" />\n";
      echo " mailed? - ".$race_pass_mailed_selector."\n";
      echo "<br />";
      echo " adv_src - <input type=\"text\" value=\"\" name=\"race_pass_adv_src\" />\n";
      $race_pass_lapz = preg_split("/ /i", $cart_qtys[5] );
      echo " laps - <input size=\"3\" type=\"text\" value=\"".$race_pass_lapz[1]."\" name=\"race_pass_laps\" />\n";
      echo " \$_paid - <input size=\"3\" type=\"text\" value=\"".$cart_qtys[3]."\" name=\"race_pass_paid\" />\n";
      echo "<hr />";
      echo " notes - <textarea name=\"race_pass_notes\" rows=\"2\" cols=\"60\">Paid-\$".$cart_qtys[3]."  Reg \$".$cart_qtys[4]."\n".$row[1][38]."</textarea>\n";
      echo "<hr />";
      $cart_qtys[1] = "0000-00-00";
      echo "upgrades...<br />";
      echo " type - ".$race_pass_upgrade_type_selector."\n";
      echo " old_pass_num - <input type=\"text\" value=\"\" name=\"race_pass_old_num\" />\n";
      echo "<br />\n";
      echo " \$_paid - <input type=\"text\" value=\"0.00\" name=\"race_pass_upgrade_paid\" />\n";
      echo " date - <input type=\"text\" value=\"".$cart_qtys[1]."\" name=\"race_pass_upgrade_date\" />\n";
      echo "<hr />";
      echo " insurance_\$_paid - <input type=\"text\" value=\"0.00\" name=\"insurance_paid\" />\n";
      echo " ins_paid_date - <input type=\"text\" value=\"".$cart_qtys[1]."\" name=\"ins_paid_date\" />\n";
      echo "<hr />";
      echo " video_\$_paid - <input type=\"text\" value=\"0.00\" name=\"video_paid\" />\n";
      echo " video_paid_date - <input type=\"text\" value=\"".$cart_qtys[1]."\" name=\"video_paid_date\" />\n";
// pass a few cart variable straight thru...
      echo " <input type=\"hidden\" value=\"".$row[1][2]."\" name=\"order_username\" />\n";
      echo " <input type=\"hidden\" value=\"".$row[1][3]."\" name=\"order_ordernum\" />\n";
      echo " <input type=\"hidden\" value=\"".$row[1][8]."\" name=\"order_cart\" />\n";
      echo " <input type=\"hidden\" value=\"".$row[1][37]."\" name=\"order_extra\" />\n";
      echo " <input type=\"hidden\" value=\"".$row[1][38]."\" name=\"order_message\" />\n";
      echo " <input type=\"hidden\" value=\"".$row[1][39]."\" name=\"order_edata\" />\n";
      echo " <input type=\"hidden\" value=\"".$_POST[confirm_this_record]."\" name=\"confirm_this_record\" />\n";

      echo "<input class=\"submitButton\" name=\"submit\" type=\"submit\" value=\"Confirm...\" />\n";
      echo "</form>";
      echo "<br/></div>\n<br/>\n<br/>\n<br/>\n";
      }  //end while 'cart_qtys' rows
    }  //end while 'cart_items' rows
 }  //end while 'orders' rows
   





?> 
