<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2011-03-19  -  cert_completion.php
#  
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
// sending header info interferes with the PDF creating...
//session_start();
//if(empty($_SESSION['userId'])){
//    header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
//    echo "\$_SESSION['userId'] error";
//}
//echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";


require_once('fpdf.php'); 
require_once('fpdi.php'); 

include("config.php");
include("ssxDBfunctions.php");

$con = mysql_connect($DBhost,$DBuser,$DBpass) or die("Unable to connect to database $DBName");
$db  = mysql_select_db($DBName,$con) or die("Unable to select database $DBName");

//$edit_id=0; if( $_GET[id] ){ $edit_id=$_GET[id]; } if( $_POST[id] ){ $edit_id=$_POST[id]; } // POST takes priority!?!
$class_date=0; if( $_GET[date] ){ $class_date =$_GET[date]; } if( $_POST[date] ){ $class_date =$_POST[date]; } // POST takes priority!?!
$class_time=0; if( $_GET[time] ){ $class_time =$_GET[time]; } if( $_POST[time] ){ $class_time =$_POST[time]; } // POST takes priority!?!
//$race_passes_sql_query = "select * from race_passes where id = '".$edit_id."' ";
$race_passes_sql_query = "select * from race_passes where class_date = '".$class_date."' and class_time = '".$class_time."' ";
$race_passes_query=mysql_query( $race_passes_sql_query, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$rows = fetch_all( $race_passes_query );


$pdf =& new FPDI(); 
$pdf->AddFont('OldEnglish-Regular','','oldengl.php');
$pdf->AddFont('BankGothicBT-Medium','','bankgothicmedium.php');
$pdf->SetAutoPageBreak( false );

 while($row = each($rows) ) 
  {
$cert_template = 'blanks/'.$_POST[location].'/LARXCert-Drive_rot.pdf';
if ( preg_match( "/comp/i", $row[1][race_pass_type] )){ $cert_template = 'blanks/'.$_POST[location].'/LARXCert-Comp_rot.pdf'; }
if ( preg_match( "/ride/i", $row[1][race_pass_type] )){ $cert_template = 'blanks/'.$_POST[location].'/LARXCert-Ride_rot.pdf'; }

$pagecount = $pdf->setSourceFile( $cert_template ); 
$tplidx = $pdf->importPage(1, '/MediaBox'); 
 
$pdf->addPage( 'L', 'letter' ); 
$pdf->useTemplate($tplidx, 0, 0); 
$pdf->SetFont('BankGothicBT-Medium','',30);

if( $row[1][class_date] == '0000-00-00' ){ $row[1][class_date] = 'TBD'; }
if( $row[1][class_time] == '00:00:00' ){ $row[1][class_time] = ''; }

$pdf->SetXY( 0 , 80 );
$pdf->Cell(280 , 15 , $row[1][driver_name], 0, 0, 'C', false);

$date_output=date("jS \d\a\y \of F, Y",strtotime ( $row[1][class_date] ));

$pdf->SetFont('OldEnglish-Regular','',20);
$pdf->SetXY( 0 , 170 );
$pdf->Cell(280 , 15 , $date_output, 0, 0, 'C', false);


 }  //end while 'race_passes' rows



$file_name = "certz_of_comp/LARX_certz_of_comp_".$class_date."_".$class_time.".pdf";
//$file_name = "certz_of_comp/LARX_certz_of_comp_".$row[1][id]."-".$row[1][driver_name]."-[".time()."]-".$row[1][race_pass_type].".pdf";
	 
$pdf->Output($file_name, 'F'); //saves as a file on the server
$pdf->Output($file_name, 'D'); //forces a download in the browser


mysql_close( $con );

?> 