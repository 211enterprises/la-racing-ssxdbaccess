<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2012-05-16  -  Class Schedule Confirmation part 'A'  -  class_conf_a.php
#  
#  simple premise...called from class sched b [same page as class contact sheets] script goes
#  thru and sends an email to BOTH email records on file [if different] with details on pending 
#  class date.
#  
#  
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
session_start();
if(empty($_SESSION['userId'])){
	header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
	echo "\$_SESSION['userId'] error";
}

if( $_SESSION['accessLevel'] == md5('admin') ||  $_SESSION['accessLevel'] == md5('moderator') ||  $_SESSION['accessLevel'] == md5('user') )
{}
else
  {
	header('location: ../fPDF/side_door_order_page.php?ref='.$_SERVER['PHP_SELF']);
  }

echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";



include("config.php");
include("ssxDBfunctions.php");

// from ssxDBfunctions.php
if( $month > 1 ){ $month--;}
elseif ( $month = 1 ){ $month = 12; $year--; }

$con = mysql_connect($DBhost,$DBuser,$DBpass) or die("Unable to connect to database $DBName");
$db  = mysql_select_db($DBName,$con) or die("Unable to select database $DBName");

if( !isset($_GET[location]) || $_GET[location] == '' ){$_GET[location] = 'LosAngeles';}


$sql_thisdate   = "select * from race_passes where location = '".$_GET[location]."' and class_date = '".$_GET[class_date]."' order by class_time, race_pass_type, race_pass_group, id ";
$query_thisdate=mysql_query( $sql_thisdate, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$rows_thisdate = fetch_all( $query_thisdate );






   echo "Locations: <a style=\"color:#000000;\" href=\"".$_SERVER['PHP_SELF']."?class_date=".$_GET[class_date]."&location=LosAngeles\">Los Angeles</a> | <a style=\"color:#000000;\" href=\"".$_SERVER['PHP_SELF']."?class_date=".$_GET[class_date]."&location=Nashville\">Nashville</a>";
   
      echo "\n<form style=\"display:inline;margin:0;padding:0;\" action=\"class_contact_list.php\" target=\"new\" method=\"post\">\n";
      echo " <input type=\"hidden\" value=\"".$_GET[class_date]."\" name=\"class_date\" />\n";
      echo " <input type=\"hidden\" value=\"".$_GET[location]."\" name=\"location\" />\n";
      echo "<input style=\"display:inline;\" class=\"submitButton\" name=\"submit\" type=\"submit\" value=\"Get Class Contact Sheet\" />\n";
      echo "</form>\n";

      echo "\n<form style=\"display:inline;margin:0;padding:0;\" action=\"#\" target=\"new\" method=\"post\">\n";
      echo " <input type=\"hidden\" value=\"".$_GET[class_date]."\" name=\"class_date\" />\n";
      echo " <input type=\"hidden\" value=\"".$_GET[location]."\" name=\"location\" />\n";
      echo "<input style=\"display:inline;\" class=\"submitButton\" name=\"submit\" type=\"submit\" value=\"Send Class Confirmation Emails\" />\n";
      echo "</form>\n";



$now=date("Y-m-d H:i:s");
$today=date("Y-m-d");
$date_parts = explode( "-", $_GET[class_date] );
$_GET[class_date] = date('F j, Y', mktime(0, 0, 0, $date_parts[1], $date_parts[2], $date_parts[0]));

  $from_us = 'L.A. Racing Scheduling<support@laracingx.com>';
  $bcc = 'vouchers@laracingx.com';

  $extra_headers  = "From: ".$from_us." \r\n";
  $extra_headers .= "Reply-To: ".$from_us." \r\n";
  $extra_headers .= "Bcc: ".$bcc." \r\n";
  $extra_headers .= "X-Priority: 2\r\nX-MSmail-Priority: high\r\n"; 

 while( count( $rows_thisdate ) > 0 && $row = each($rows_thisdate ) ) 
  {
  $row[1]['driver_name'] = stripslashes($row[1]['driver_name']);
  $subject = "L.A.Racing - ".$row[1]['class_date']. " Class Confirmation - ".$row[1]['driver_name']." - ".$row[1][id];

  $text  = "\r\n";
  $text .= "Driver Name:          ".$row[1]['driver_name']." \r\n\r\n";
  $text .= "Scheduled Class Date: ".$row[1]['class_date']." \r\n";
  $text .= "Scheduled Class Time: ".$row[1]['class_time']." \r\n";
  $text .= "Race Pass #:          ".$row[1]['id']." \r\n";
  $text .= "Location:             ".$row[1][location]." \r\n\r\n";


   

//$is_numeric = preg_match('/[\+\-\:\(\)\d]+/', $find_this);
//$is_alpha   = preg_match('/[a-zA-Z]+/', $find_this);

  $text .= " - PLEASE REPLY TO THIS MESSAGE OR CALL US @ 888-LA-RACING [527-2246] TO CONFIRM YOUR RESERVATION.\r\n\r\n\r\n";
  $text .= " - Please arrive about 15-30 mins early for check-in. Class begins at the posted time. Late arrivals may be rescheduled to a later date and/or time. If you cannot make it please let us know 72 hrs in advance to avoid any rescheduling fees or forfeiture of your race pass.\r\n\r\n";
  $text .= " - Please have a valid drivers license with you. Minor drivers [under 18] will also need at least one parent/guardian present.\r\n\r\n";

  if( preg_match('/VIP/', $row[1]['race_pass_type']) )
    {
    $text .= " - We do offer in-car videos packages starting at $79.00 and a limited liability insurance for $89.00. The Insurance is a requirement for all VIP race passes.\r\n\r\n";
    }
  else
    {
    $text .= " - We do offer in-car videos packages starting at $79.00 and optional limited liability insurance for $89.00. Drivers opting out of the Insurance may be fully responsible for any and all damages.\r\n\r\n";
    }
  
  $text .= " - Please wear closed-toe shoes (tennis shoes are fine) and socks to cover all skin below the firesuit. Also, avoid slip-on style shoes as they may easily slip-off in the car.\r\n\r\n";
  $text .= " - Firesuit [SFI 3-2A/5], helmet [SA 2005] and gloves [SFI 3.3] are provided. You may bring your own equipment IF it meets or exceeds current minimum standards. Motorcycle helmets are generally not acceptable..\r\n\r\n";
  $text .= " - Merchandise and Food are available on site.\r\n\r\n\r\n";
  $text .= " - You will/have received a confirmation email for EACH race pass scheduled for this class. It is possible that others in your group have received the confirmation at a different email. Please call us @ 888-LA-RACING [527-2246] to confirm if it appears any are missing.\r\n\r\n";
  $text .= " - If you have any questions you can email or call us @ 888-LA-RACING [527-2246].\r\n\r\n";


  $adding = "~~~<br />\n~~~".$now." - ...class confirmation email sent thru system...";
  if( mail($_POST['email'], $subject, $text, $extra_headers) )
    {
    $sql_query = "update race_passes set race_pass_notes = concat( race_pass_notes ,'$adding' )  where id = '".$row[1][id]."' ";
    $query=mysql_query( $sql_query, $con )  or die('Could NOT Update DB:: ' . mysql_error());
    }
  }  //end while 'orders' rows
   


mysql_close( $con );


      echo "<script language=\"JavaScript\" type=\"text/javascript\">\n";
      echo "<!--\n";
      echo "  self.parent.opener.window.location.reload();\n";
      echo "document.writeln( window.opener );";
      echo "// -->\n";
      echo "</script>\n";

echo "<body onload=\"window.close();\">\n\n\n";


?> 
