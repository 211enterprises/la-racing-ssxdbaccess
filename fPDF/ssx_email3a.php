<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2010-12-09   -  ssx_pass.php --> ssx_email.php
#  Taking basic PDF creation functionality and extending it by adding direct to email address 
#  on record delivery capabilities.
#  
#  
      echo " <input type=\"hidden\" value=\"".$row[1][driver_email1]."\" name=\"driver_email1\" />\n";
      echo " <input type=\"hidden\" value=\"".$row[1][driver_email2]."\" name=\"driver_email2\" />\n";
#  
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
// sending header info interferes with the PDF creating...
//session_start();
//if(empty($_SESSION['userId'])){
//    header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
//    echo "\$_SESSION['userId'] error";
//}
//echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";


require_once('fpdf.php'); 
require_once('fpdi.php'); 

include("config.php");
include("ssxDBfunctions.php");

$con = mysql_connect($DBhost,$DBuser,$DBpass) or die("Unable to connect to database $DBName");
$db  = mysql_select_db($DBName,$con) or die("Unable to select database $DBName");
$sql            ="select *                   from Admin           where       AdminName='$_POST[username]' and AdminPassword='$_POST[password]' ";
$sql_statement = "select race_pass_template from race_pass_types where race_pass_type ='$_POST[race_pass]' ";
$query=mysql_query( $sql_statement, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$row = mysql_fetch_row( $query );
mysql_close( $con );


$pdf =& new FPDI(); 
$race_pass_template = 'blanks/'.$row[0];

$pagecount = $pdf->setSourceFile( $race_pass_template ); 
$tplidx = $pdf->importPage(1, '/ArtBox'); 
 
$pdf->addPage(); 
$pdf->useTemplate($tplidx, 2, 1, 214); 
$pdf->AddFont('BankGothicBT-Medium','','bankgothicmedium.php');
$pdf->SetFont('BankGothicBT-Medium','',18);
$pdf->SetXY(126,54);
$pdf->Cell(86 , 10 , $_POST['driver_name'], 0, 0, 'C', false);
$pdf->SetFont('BankGothicBT-Medium','',16);
$pdf->SetXY(126,65);
if( $_POST['class_date'] == '0000-00-00' || $_POST['class_date'] == '9999-12-31' ){ $_POST['class_date'] = 'TBD'; }
$pdf->Cell(86 , 10 , $_POST['class_date'], 0, 0, 'C', false);
$pdf->SetXY(39,109);
$pdf->Cell(65 , 10 , $_POST['expires'], 0, 0, 'C', false);
$pdf->SetXY(39,114);
$pdf->Cell(65 , 10 , $_POST['pass_num'], 0, 0, 'C', false);

$file_name = "LARX Race Pass ".$_POST['pass_num']."-".$_POST['driver_name']."-[".time()."]-".$_POST['race_pass'].".pdf";
	 
$pdf->Output("race_passes/".$file_name, 'F'); //saves as a file on the server
//$pdf->Output($file_name, 'D'); //forces a download in the browser

################
################
################

$attachment = chunk_split(base64_encode(file_get_contents("race_passes/".$file_name)));
$random_hash = md5(date('r', time()));

$from_us = 'L.A. Racing <support@laracingx.com>';
$bcc = 'scott@laracingx.com,darlene@laracingx.com';
$subject .= "L.A. Racing Experience - Race Pass #".$_POST['pass_num']. "- ".$_POST['driver_name']."\n";

$extra_headers  = "From: ".$from_us." \r\n";
$extra_headers .= "Reply-To: ".$from_us." \r\n";
$extra_headers .= "Bcc: ".$bcc." \r\n";
$extra_headers .= "Content-Type: multipart/mixed;\r\n  boundary=\"PHP-mixed-".$random_hash."\"\r\n"; 

$text = '';
//$text .= "This is a multi-part message in MIME format.\r\n";

$text .= "--PHP-mixed-".$random_hash."\r\n";
//$text .= "Content-Type: multipart/alternative; boundary=\"PHP-alt-".$random_hash."\"\r\n\r\n";


//$text .= "--PHP-alt-".$random_hash."\r\n";
$text .= "--PHP-mixed-".$random_hash."\r\n";
$text .= "Content-Type: text/plain; charset=utf-8\r\n";
$text .= "Content-Transfer-Encoding: 7bit\r\n";

if( $_POST['class_date'] == 'TBD' ){$_POST['class_date'] = 'To Be Determined'; }
if( $_POST['class_time'] == '00:00:00' ){$_POST['class_time'] = 'To Be Determined'; }
$text  .= "\r\n";
$text .= "Driver Name:          ".$_POST['driver_name']." \r\n\r\n";
$text .= "Scheduled Class Date: ".$_POST['class_date']." \r\n";
$text .= "Scheduled Class Time: ".$_POST['class_time']." \r\n";
$text .= "Race Pass #:          ".$_POST['pass_num']." \r\n\r\n";

$text .= "\r\n\r\nPlease check the class schedule for available dates when you are ready to schedule or if you might need to reschedule at http://laracingx.com/schedule.htm \r\n\r\n";


//$text .= "--PHP-alt-".$random_hash."--\r\n";

$text .= "--PHP-mixed-".$random_hash."\r\n";
$text .= "Content-Type: application/pdf; name=\"".$file_name."\"\r\n";
$text .= "Content-Transfer-Encoding: base64\r\n";
$text .= "Content-Disposition: attachment\r\n\r\n";

$text .= $attachment."\n\n";
$text .= "--PHP-mixed-".$random_hash."--\r\n";



//ob_start(); //Turn on output buffering
//$text = ob_get_clean();

$output = "";
if( mail($_POST['driver_email1'], $subject, $text, $extra_headers) )
  {
  $output .= "<p>Thank You! Drive Thru!.<p>";
  $output .= "<a href=\"contact.htm\" onClick=\"window.close();\">Close</a> this window.";
  }
else
  {
  $output .= "E-mail Error. Message Not Sent<br><br>Please re-try or contact us by phone @ 877-901-RACE [7223]<br><br>Our apologies for the inconvenience.";
  }

echo $output."\n\n\n";
//echo $text;


?> 