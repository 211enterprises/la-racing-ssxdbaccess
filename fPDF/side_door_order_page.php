<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2009-10-14  -  Side Door / Back Door / Quick Order Page
#  page pulls products data from DB, presents it to the user [intended to be internal sales rep]
#  in such a manner as to quickly facilitate processing ALL orders as web orders. Adjustable QTY
#  && Price fields made available. All available class dates are also made available, including 
#  closed classes! Gotta have the power to over-ride the usual... Actually closed classes from
#  the current and previous months are available to "GO BACK AND FIX" stuff from end of month
#  class dates in the event that the oh shit factor rears its head.
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
session_start();
if(empty($_SESSION['userId'])){
	header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
	echo "\$_SESSION['userId'] error";
}
echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";


include("config.php");
include("ssxDBfunctions.php");

$year = date('Y');
$month= date('m');
if( $month > 1 ){ $month--;}
elseif ( $month = 1 ){ $month = 12; $year--; }

$accessLevel = '';
if( $_SESSION['accessLevel'] == md5('admin') )    { $accessLevel = 'admin_disc'; }
if( $_SESSION['accessLevel'] == md5('moderator') ){ $accessLevel = 'moderator_disc'; }
if( $_SESSION['accessLevel'] == md5('user') )     { $accessLevel = 'user_disc'; }
if( $_SESSION['accessLevel'] == md5('sales') )    { $accessLevel = 'sales_disc'; }
if( $_SESSION['accessLevel'] == md5('agent') )    { $accessLevel = 'agent_disc'; }
if( $_SESSION['accessLevel'] == md5('barter') )   { $accessLevel = 'barter_disc'; }




$sql             ="select *                   from Admin           where       AdminName='$_POST[username]' and AdminPassword='$_POST[password]' ";
$sql_racepasses  = "select * from race_pass_types where $accessLevel IS NOT NULL ";
$sql_opendates   = "select * from training_dates where is_private = 'false' AND class_status = 'open' order by date asc ";
$sql_closeddates = "select * from training_dates where is_private = 'false' AND class_status = 'full' and date >= '".$year."-".$month."-01' order by date desc ";

$con = mysql_connect($DBhost,$DBuser,$DBpass) or die("Unable to connect to database $DBName");
$db  = mysql_select_db($DBName,$con) or die("Unable to select database $DBName");

$query_racepasses=mysql_query( $sql_racepasses, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$query_opendates=mysql_query( $sql_opendates, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$query_closeddatess=mysql_query( $sql_closeddates, $con )  or die('Could not Pull From DB:: ' . mysql_error());

$rows_racepasses = fetch_all( $query_racepasses );
$rows_opendates = fetch_all( $query_opendates );
$rows_closeddatess = fetch_all( $query_closeddatess );

mysql_close( $con );

$open_dates = "  <option  value=\"  Date: TBD [to be determined]\">To Be Determined / Decide Later</option>\n";

echo color_table_style();

echo "<title>LARX Quick Order Page</title>\n";

if( !isset( $_GET[location] )){$_GET[location] = 'LosAngeles'; }
else if( $_GET[location] == '' ){ $_GET[location] = 'LosAngeles'; }

   echo "<div style=\"width:700px;\"><form action=\"http://ww8.aitsafe.com/cf/addmulti.cfm\" method=\"post\">\n";

echo "\n\n<table><tr class=\"".$_GET[location]."\"><td>\n";

//   echo " <input type=\"hidden\" value=\"http://ssxdbaccess.srvthis.com/fPDF/test_read.html\" name=\"return\" />\n";
   echo " <input type=\"hidden\" value=\"A6117663\" name=\"userid\" />\n";
   echo " <input class=\"submitButton\" name=\"submit\" type=\"submit\" value=\"Sign 'Em UP!\" />";

if( $_GET[location] == 'LosAngeles' ){ echo "<a href=\"".$_SERVER['PHP_SELF']."?location=Nashville\">FOR NASHVILLE!</a>\n"; }
else if( $_GET[location] == 'Nashville' ){ echo "<a href=\"".$_SERVER['PHP_SELF']."?location=LosAngeles\">FOR LOS ANGELES!</a>\n"; }
echo "</td></tr></table>\n<br><br>\n <hr />\n\n";

echo " <input type=\"hidden\" value=\"".$_GET[location]."\" name=\"sd\" />\n";

   while($row = each($rows_opendates) ) 
   {
   $open_dates .= "  <option  value=\"  Date: ".$row[1][2]."\">".$row[1][2]." - ".$row[1][1]."</option>\n";
   }  //end while 'training_dates' rows

   $open_dates .= "  <option  value=\"  Date: --------------\"> --- Closed Dates --- </option>\n";
   
   if( $_SESSION['accessLevel'] == md5('admin') || $_SESSION['accessLevel'] == md5('moderator') )
     {
     while($row = each($rows_closeddatess) ) 
       {
       $open_dates .= "  <option  value=\"  Date: ".$row[1][2]."\">".$row[1][2]." - ".$row[1][1]."</option>\n";
       }  //end while 'training_dates' rows
     }

   while($row = each($rows_racepasses) ) 
   {
   //print_r ($row);
//   echo $row[1][0].'  -  id';
   echo " <input type=\"hidden\" value=\"0.00\" name=\"shipping".$row[1][0]."\" />\n";
//   echo " <input type=\"hidden\" value=\"1\" name=\"addqty".$row[1][0]."\" />\n";
   echo " <input type=\"text\" size=\"4\" value=\"0\" name=\"qty".$row[1][0]."\" />\n";
   echo " - ".$row[1][1]." - \n";
   echo " <input type=\"text\" size=\"5\" value=\"".$row[1][3]."\" name=\"price".$row[1][0]."\" />\n";
   echo " <input type=\"hidden\" value=\"".$row[1][3]."\" name=\"units".$row[1][0]."\" />\n";
   echo " <input type=\"hidden\" value=\"".$row[1][1]."\" name=\"scode".$row[1][0]."\" />\n";
   echo " <input type=\"hidden\" value=\"".$row[1][1]."\" name=\"product".$row[1][0]."[]\" />\n";




if(  substr($row[1][1],-3 )  == "VIP"  )
   {
   echo " <input type=\"hidden\" value=\" Insurance Required at check-in \" name=\"product".$row[1][0]."[]\" />\n";
   }
   echo " <input type=\"hidden\" value=\"".$row[1][1]."\" name=\"product".$row[1][0]."[]\" />\n";

   echo " <select style=\"float:right;\" name=\"product".$row[1][0]."[]\">\n";
   echo $open_dates." \n";
   echo " </select>\n <br /><br /><hr />\n";

/*
   echo " <select style=\"float:right;\" name=\"sd".$row[1][0]."\">\n";
   echo   "<option  value=\"LosAngeles\"> Los Angeles </option> \n";
   echo   "<option  value=\"Nashville\"> Nashville </option> \n";
   echo " </select>\n <br /><br /><hr />\n";
*/
   }  //end while 'race_pass_types' rows

   echo " <input class=\"submitButton\" name=\"submit\" type=\"submit\" value=\"Sign 'Em UP!\" />\n <br />\n</form></div>";


?> 

