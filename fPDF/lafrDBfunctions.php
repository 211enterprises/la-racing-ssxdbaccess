<?PHP






function adv10_contribution() {  return 50;  }
function adv20_contribution() {  return 100;  }
function adv40_contribution() {  return 150;  }


function sales_report($result) 
  {
  while($row=mysql_fetch_array($result)) 
    {
    $return[] = $row;
    }
  return $return;
  }



function get_date_ranged_select_data( $range_from, $range_to, $selected_item ) 
  {
  $sel_data = '';
  foreach( range( $range_from, $range_to ) as $date_numbers )
    {
    $sel='';
    if( $date_numbers == $selected_item ) { $sel = 'selected'; } 
    if( $date_numbers*1 < 10 ) { $date_numbers = '0'.$date_numbers; }
    $sel_data .= "        <option ".$sel." value=\"".$date_numbers."\">".$date_numbers."</option>\n";
    }
  return $sel_data;
  } // end get_day_select_data()


function get_orgz_pairz_select_data( $rows_orgz_only, $rows_orgz_pairz ) 
  {
  $sel_data = '';
  while( count( $rows_orgz_only ) > 0 && $row_orgz_only = each($rows_orgz_only ) ) 
    {
    // where sd like abc
    //    $extra_liken .= " or created LIKE '%".$find_this."%' ";
    $sel_data .= "        <option value=\" and sd like \\'%".$row_orgz_only[1][org_namez]."%\\' \"> ---- ".$row_orgz_only[1][org_namez]." -- ALL TEAMS ---- </option>\n";
    for ( $i = 0; $i <= count( $rows_orgz_pairz ); $i++ )
      {
      if( strcmp( $row_orgz_only[1][org_namez],$rows_orgz_pairz[$i][org_namez] ) == 0 )
        {
        // where sd like abc AND sd like xyz deptz
        // compund clasue uses ALL CAPS 'AND', both start with lowercase
        $sel_data .= "        <option value=\" and sd like \\'%".$rows_orgz_pairz[$i][org_namez]."%\\' AND  sd like \\'%".$rows_orgz_pairz[$i][dept_namez]."%\\' \">".$rows_orgz_pairz[$i][org_namez]." - ".$rows_orgz_pairz[$i][dept_namez]."</option>\n";
        }
      }  //end while 'rows_orgz_pairz' rows
    }  //end while 'rows_orgz_only' rows
  return $sel_data;
  } // end get_day_select_data()


function get_sd_orgz( $rows_thisdate ) 
  {
  $sd_orgz = array();
  while( count( $rows_thisdate ) > 0 && $row = each($rows_thisdate ) ) 
    {
    $sd_vals = preg_split  ( '/:::/' , $row[1][sd] );
    $sd_orgz[$sd_vals[0]] = $sd_vals[0];
    }
  return $sd_orgz;
  }

function get_sd_deptz( $rows_thisdate,$sd_org ) 
  {
  $sd_deptz = array();
  while( count( $rows_thisdate ) > 0 && $row = each($rows_thisdate ) ) 
    {
    $sd_vals = preg_split  ( '/:::/' , $row[1][sd] );
    if( $sd_org[0] == $sd_vals[0] )
      {
      $sd_deptz[$sd_vals[1]] = $sd_vals[1];
      }
    }
  return $sd_deptz;
  }

function get_sd_studentz( $rows_thisdate,$sd_dept ) 
  {
  $sd_student = array();
  while( count( $rows_thisdate ) > 0 && $row = each($rows_thisdate ) ) 
    {
    $sd_vals = preg_split  ( '/:::/' , $row[1][sd] );
    if( $sd_dept == $sd_vals[1] )
      {
      $sd_student[$sd_vals[2]] = $sd_vals[2];
      }
    }
  return $sd_student;
  }


function get_pass_tally( $rows_thisdate,$pass_to_tally,$sd_dept,$sd_student ) 
  {
  $pass_tally = 0;
  while( $row = each( $rows_thisdate ) )
    {  //if (preg_match("/.mals-e./i", gethostbyaddr($_SERVER['REMOTE_ADDR']))) {
    if( preg_match( "/".$pass_to_tally."/i" , $row[1][cart] )  &&  preg_match( "/".$sd_dept."/i" , $row[1][sd] )  &&  preg_match( "/".$sd_student."/i" , $row[1][sd] ) )
      {
      $cart_items = preg_split  ( '/~/' , $row[1]['cart'] ); /* if multiple cart items, i.e. Adv10 & Adv20, split them */ 
      while(  count( $cart_items ) > 0 && $cart_item = each( $cart_items ) )
        {
        if( preg_match( "/".$pass_to_tally."/i" , $cart_item[1] ) )
          {
          $cart_qtys = preg_split  ( '/:/' , $cart_item[1] );
          $pass_tally += $cart_qtys[2]*1;
          // echo "\n"; print_r( $cart_qtys ); echo "\n\n";
          }
        }
      }
    }
  return $pass_tally;
  } // end get_pass_tally()




function xml_templ_salesreport( $rows_thisdate,$_POST ) 
  {
  $output_cache = '';
  $output_cache .= '<?xml version="1.0" ?>'."\n";
  $output_cache .= '<!DOCTYPE note SYSTEM "DB_Report_Output.dtd">'."\n";
  $output_cache .= '<?xml-stylesheet type="text/xsl" href="DB_Reports_def.xsl"?>'."\n";
  $output_cache .= '<sales_report>'."\n";
  $output_cache .= xml_templ_period( $_POST );
  $sd_orgz = get_sd_orgz( $rows_thisdate );
  if( count( $sd_orgz ) > 0 ) 
    {
    while( $sd_org = each($sd_orgz ) )
      {
      $output_cache .= xml_templ_school( $rows_thisdate,$sd_org );
      }
    }
  $output_cache .= '</sales_report>'."\n";
    //  XML File output...
  $file_name = "xml/LARF_xml_Report_".time().".xml";
  $fileHandle =fopen($file_name,"x")or exit("Unable to create file \n $file_name !!!"); // open file in write only mode...do not overwrite existing
  fwrite ($fileHandle, $output_cache);
  fclose ($fileHandle); 
    //  output link to XML report AND primary/secondary contacts for each team
  echo '<p>View results as <a href="'.$file_name.'" target="new_xml">XML</a></p>';
  } // end xml_templ_salesreport()




function disp_contact_data( $rows_thisdate,$rows_orgz_contactz ) 
  {
    $i = 0;
  $sd_orgz = get_sd_orgz( $rows_thisdate );
  while( $sd_org = each( $sd_orgz ) )
    {
    $sd_deptz = get_sd_deptz( $rows_thisdate,$sd_org );

    while( $contact = each( $rows_orgz_contactz ) )
      {
      echo "\n While rows_orgz_contactz --- contact[1][org_namez] - '".$contact[1][org_namez]."' --- contact[1][dept_namez] -'".$contact[1][dept_namez]."'\n";
      while( $sd_dept = each( $sd_deptz ) )
        {
        echo "\n             While sd_deptz  - '".$sd_org[1]."' - '".$sd_dept[0]."'\n";
            //print_r( $sd_dept );
        }
      }
    }
  } // end xml_templ_salesreport()


function xml_templ_period( $_POST ) 
  {
  $output_cache = '';
  $report_from=$_POST[report_from_year].'-'.$_POST[report_from_month].'-'.$_POST[report_from_day];
  $report_to  =$_POST[report_to_year].'-'.$_POST[report_to_month].'-'.$_POST[report_to_day];
  $output_cache .= '  <period>'."\n";
  $output_cache .= '    <all>true</all>'."\n";
  $output_cache .= '    <from>'.$report_from.'</from>'."\n";
  $output_cache .= '    <to>'.$report_to.'</to>'."\n";
  $output_cache .= '  </period>'."\n";
  return $output_cache;
  } // end xml_templ_period()



      //
      //  <school_totals> section depends on ALL other segments being cycled
      //  through and passing data back up the incurssion chain or extracting
      //  the data multiple times...now and for each section that needs subtotals
      //  neither is very pretty!!!
      //
function xml_templ_school( $rows_thisdate,$sd_org ) 
  {
  $output_cache = '';
    // function get_pass_tally( $rows_thisdate,$pass_to_tally,$sd_dept,$sd_student ) 
  $adv10_tally = get_pass_tally( $rows_thisdate,'Adventure 10',':::',':::' );
  $adv20_tally = get_pass_tally( $rows_thisdate,'Adventure 20',':::',':::' );
  $adv40_tally = get_pass_tally( $rows_thisdate,'Adventure 40',':::',':::' );
  $total_passes = $adv10_tally*1+$adv20_tally*1+$adv40_tally*1;
  $total_contribution = $adv10_tally*adv10_contribution()*1+$adv20_tally*adv20_contribution()*1+$adv40_tally*adv40_contribution()*1;
  
  $output_cache .= '  <school>'."\n";
  $output_cache .= '    <school_name>'.$sd_org[0].'</school_name>'."\n";
//      print_r( $rows_thisdate );
  $output_cache .= '    <school_totals>'."\n";
  $output_cache .= '      <school_all_passes>'.$total_passes.'</school_all_passes>'."\n";
  $output_cache .= '      <school_adv10>'.$adv10_tally.'</school_adv10>'."\n";
  $output_cache .= '      <school_adv20>'.$adv20_tally.'</school_adv20>'."\n";
  $output_cache .= '      <school_adv40>'.$adv40_tally.'</school_adv40>'."\n";
  $output_cache .= '      <school_contribution>$'.$total_contribution.'</school_contribution>'."\n";
  $output_cache .= '    </school_totals>'."\n";
  $sd_deptz = get_sd_deptz( $rows_thisdate,$sd_org );
  if( count( $sd_deptz ) > 0 ) 
    {
    while( $sd_dept = each( $sd_deptz ) )
      {
       //    echo "\n"; print_r( $sd_dept ); echo "\n\n";
      $output_cache .= xml_templ_team( $rows_thisdate,$sd_org,$sd_dept[1] );
      }
    }
  $output_cache .= '  </school>'."\n";
  return $output_cache;
  } // end xml_templ_school()


function xml_templ_team( $rows_thisdate,$sd_org,$sd_dept ) 
  {
  $output_cache = '';
    // function get_pass_tally( $rows_thisdate,$pass_to_tally,$sd_dept,$sd_student ) 
  $adv10_tally = get_pass_tally( $rows_thisdate,'Adventure 10',$sd_dept,':::' );
  $adv20_tally = get_pass_tally( $rows_thisdate,'Adventure 20',$sd_dept,':::' );
  $adv40_tally = get_pass_tally( $rows_thisdate,'Adventure 40',$sd_dept,':::' );
  $total_passes = $adv10_tally*1+$adv20_tally*1+$adv40_tally*1;
  $total_contribution = $adv10_tally*adv10_contribution()*1+$adv20_tally*adv20_contribution()*1+$adv40_tally*adv40_contribution()*1;
  
  $output_cache .= '    <team>'."\n";
  $output_cache .= '      <team_name>'.$sd_dept.'</team_name>'."\n";
  $output_cache .= '      <team_totals>'."\n";
  $output_cache .= '        <team_all_passes>'.$total_passes.'</team_all_passes>'."\n";
  $output_cache .= '        <team_adv10>'.$adv10_tally.'</team_adv10>'."\n";
  $output_cache .= '        <team_adv20>'.$adv20_tally.'</team_adv20>'."\n";
  $output_cache .= '        <team_adv40>'.$adv40_tally.'</team_adv40>'."\n";
  $output_cache .= '        <team_contribution>$'.$total_contribution.'</team_contribution>'."\n";
  $output_cache .= '      </team_totals>'."\n";
  $sd_studentz = get_sd_studentz( $rows_thisdate,$sd_dept );
      //     echo "\n"; print_r( $sd_studentz ); echo "\n\n";
  if( count( $sd_studentz ) > 0 ) 
    {
    while( $sd_student = each( $sd_studentz ) )
      {
      $output_cache .= xml_templ_student( $rows_thisdate,$sd_org,$sd_dept,$sd_student );
      }
    }
  $output_cache .= '    </team>'."\n";
  return $output_cache;
  } // end xml_templ_team()


function xml_templ_student( $rows_thisdate,$sd_org,$sd_dept,$sd_student )
  {

  $output_cache = '';
    // function get_pass_tally( $rows_thisdate,$pass_to_tally,$sd_dept,$sd_student ) 
  $adv10_tally = get_pass_tally( $rows_thisdate,'Adventure 10',$sd_dept,$sd_student['value'] );
  $adv20_tally = get_pass_tally( $rows_thisdate,'Adventure 20',$sd_dept,$sd_student['value'] );
  $adv40_tally = get_pass_tally( $rows_thisdate,'Adventure 40',$sd_dept,$sd_student['value'] );
  $total_passes = $adv10_tally*1+$adv20_tally*1+$adv40_tally*1;
  $total_contribution = $adv10_tally*adv10_contribution()*1+$adv20_tally*adv20_contribution()*1+$adv40_tally*adv40_contribution()*1;

  $output_cache .= '      <student>'."\n";
  $output_cache .= '        <student_name>'.$sd_student['value'].'</student_name>'."\n";
  $output_cache .= '        <student_totals>'."\n";
  $output_cache .= '          <student_all_passes>'.$total_passes.'</student_all_passes>'."\n";
  $output_cache .= '          <student_adv10>'.$adv10_tally.'</student_adv10>'."\n";
  $output_cache .= '          <student_adv20>'.$adv20_tally.'</student_adv20>'."\n";
  $output_cache .= '          <student_adv40>'.$adv40_tally.'</student_adv40>'."\n";
  $output_cache .= '          <student_contribution>$'.$total_contribution.'</student_contribution>'."\n";
  $output_cache .= '        </student_totals>'."\n";
  
  $xml_templ_passdetails = '';
  while( $row = each( $rows_thisdate ) )
    {  //if (preg_match("/.mals-e./i", gethostbyaddr($_SERVER['REMOTE_ADDR']))) {
    if( preg_match( "/".$sd_student['value']."/i" , $row[1][sd] ) && preg_match( "/".$sd_dept."/i" , $row[1][sd] ) )
      {
      $xml_templ_passdetails .= xml_templ_passdetails( $row[1],$sd_org,$sd_dept,$sd_student );
      }
    }
  $output_cache .= $xml_templ_passdetails;
  $output_cache .= '      </student>'."\n";
  return $output_cache;
  } // end xml_templ_student()


function xml_templ_passdetails( $row,$sd_org,$sd_dept,$sd_student ) 
  {

  $adv10_tally = get_pass_tally2( $row,'Adventure 10',$sd_dept,$sd_student['value'] );
  $adv20_tally = get_pass_tally2( $row,'Adventure 20',$sd_dept,$sd_student['value'] );
  $adv40_tally = get_pass_tally2( $row,'Adventure 40',$sd_dept,$sd_student['value'] );
  $total_passes = $adv10_tally*1+$adv20_tally*1+$adv40_tally*1;
  $total_contribution = $adv10_tally*adv10_contribution()*1+$adv20_tally*adv20_contribution()*1+$adv40_tally*adv40_contribution()*1;


  $xml_templ_passdetails .= '        <pass_details id="'.$row[id].'">'."\n";
  $xml_templ_passdetails .= '          <date>'.$row[created].'</date>'."\n";
  //    $pass_detailz[0] =  <date>Adventure 40   Date: TBD [to be determined] : 1 : 599 : 699.0000 : Adventure 40</date>
  $xml_templ_passdetails .= '          <receipt_num>'.$row[ordernum].'</receipt_num>'."\n";
  $xml_templ_passdetails .= '          <qty>'.$total_passes.'</qty>'."\n";
  $xml_templ_passdetails .= '          <contribution>$'.$total_contribution.'</contribution>'."\n";
  $xml_templ_passdetails .= '        </pass_details>'."\n";
  return $xml_templ_passdetails;
  } // end xml_templ_passdetails()


function get_pass_tally2( $row,$pass_to_tally,$sd_dept,$sd_student ) 
  {
           //        echo "\n".$row[id]."\n\n";

  $pass_tally = 0;
    {  //if (preg_match("/.mals-e./i", gethostbyaddr($_SERVER['REMOTE_ADDR']))) {
    if( preg_match( "/".$pass_to_tally."/i" , $row[cart] )  &&  preg_match( "/".$sd_dept."/i" , $row[sd] )  &&  preg_match( "/".$sd_student."/i" , $row[sd] ) )
      {
      $cart_items = preg_split  ( '/~/' , $row['cart'] ); /* if multiple cart items, i.e. Adv10 & Adv20, split them */ 
      while(  count( $cart_items ) > 0 && $cart_item = each( $cart_items ) )
        {
        if( preg_match( "/".$pass_to_tally."/i" , $cart_item[1] ) )
          {
          $cart_qtys = preg_split  ( '/:/' , $cart_item[1] );
          $pass_tally += $cart_qtys[2]*1;
          // echo "\n"; print_r( $cart_qtys ); echo "\n\n";
          }
        }
      }
    }
  return $pass_tally;
  } // end get_pass_tally()

/*
    [2] => Array
        (
            [id] => 189
            [validated] => true
            [username] => A1117675
            [ordernum] => 3463828
            [ip] => 65.244.225.162
            [date] => 23 Dec 2009 - 19:36
            [method] => Card
            [cart] => Adventure 10   Date: TBD [to be determined] : 1 : 274 : 299.0000 : Adventure 10
            [subtotal] => 274.00
            [total] => 274.00
            [inv_name] => Some Buyer
            [inv_company] => Some Random Company
            [inv_addr1] => 123 Main St
            [inv_addr2] => Anytown
            [inv_state] => CA
            [inv_zip] => 90210
            [inv_country] => US

            [del_name] => Some Rcvr
            [del_company] => Some Other Company
            [del_addr1] => 456 Main St
            [del_addr2] => Anothertown
            [del_state] => CA
            [del_zip] => 90211
            [del_country] => US
            [del_tel] => 333-222-1111
            [tel] => 999-888-7777
            [fax] => 888-850-2258
            [email] => nada@nada.com
            [extra] => 
            [message] => testeeeng yet again
            [edata] => 
            [sd] => Arcadia High School:::ASB:::Student ID#666
            [created] => 2009-12-23 19:36:01
        )

*/





?>