<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2009-10-14  -  student_signup_sheet_placeholder.php
#  
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
// sending header info interferes with the PDF creating...
//session_start();
//if(empty($_SESSION['userId'])){
//    header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
//    echo "\$_SESSION['userId'] error";
//}
//echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";


require_once('fpdf.php'); 
require_once('fpdi.php'); 

include("config.php");
include("ssxDBfunctions.php");

$con = mysql_connect($DBhost,$DBuser,$DBpass) or die("Unable to connect to database $DBName");
$db  = mysql_select_db($DBName,$con) or die("Unable to select database $DBName");

$edit_id=0; if( $_GET[id] ){ $edit_id=$_GET[id]; } if( $_POST[id] ){ $edit_id=$_POST[id]; } // POST takes priority!?!
$race_passes_sql_query = "select * from race_passes where id = '".$edit_id."' ";
$race_passes_query=mysql_query( $race_passes_sql_query, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$rows = fetch_all( $race_passes_query );



 while($row = each($rows) ) 
  {
$pdf =& new FPDI(); 
$race_pass_template = 'blanks/signup_sheet.pdf';

$pagecount = $pdf->setSourceFile( $race_pass_template ); 
$tplidx = $pdf->importPage(1, '/MediaBox'); 
 
$pdf->addPage(); 
$pdf->useTemplate($tplidx, 2, 1, 214); 
$pdf->AddFont('BankGothicBT-Medium','','bankgothicmedium.php');
$pdf->SetFont('BankGothicBT-Medium','',18);
$pdf->SetAutoPageBreak( false );

if( $row[1][class_date] == '0000-00-00' ){ $row[1][class_date] = 'TBD'; }
if( $row[1][class_time] == '00:00:00' ){ $row[1][class_time] = ''; }
$pdf->SetXY( 25 , 25 );
  $row[1][created] = preg_split  ( '/ /' , $row[1][created] );
//$pdf->Cell(42 , 10 , $row[1][created][0], 0, 0, 'C', false);
$pdf->SetXY( 91 , 25 );
//$pdf->Cell(35 , 10 , $row[1][class_time], 0, 0, 'C', false);
$pdf->SetXY( 152 , 25 );
//$pdf->Cell(50 , 10 , $row[1][class_date], 0, 0, 'C', false);
$pdf->SetXY( 38 , 32 );
$pdf->Cell(29 , 10 , $row[1][id], 0, 0, 'C', false);

$pdf->SetXY( 53 , 47 );
//$pdf->Cell(150 , 10 , $row[1][driver_name], 0, 0, 'L', false);
$pdf->SetXY( 53 , 54 );
//$pdf->Cell(150 , 10 , $row[1][driver_addr1], 0, 0, 'L', false);
$pdf->SetXY( 22 , 61 );
//$pdf->Cell(55 , 10 , $row[1][driver_addr2], 0, 0, 'L', false);
$pdf->SetXY( 88 , 61 );
//$pdf->Cell(45 , 10 , $row[1][driver_state], 0, 0, 'L', false);
$pdf->SetXY( 142 , 61 );
//$pdf->Cell(60 , 10 , $row[1][driver_zip], 0, 0, 'L', false);
$pdf->SetXY( 43 , 68 );
//$pdf->Cell(85 , 10 , $row[1][driver_1st_ph], 0, 0, 'L', false);
$pdf->SetXY( 148 , 68 );
//$pdf->Cell(55 , 10 , $row[1][driver_2nd_ph], 0, 0, 'L', false);
$pdf->SetXY( 21 , 75 );
//$pdf->Cell(55 , 10 , $row[1][driver_fax], 0, 0, 'L', false);
$pdf->SetXY( 43 , 82 );
//$pdf->Cell(160 , 10 , $row[1][driver_email1], 0, 0, 'L', false);



$orders_sql_query = "select * from orders where ordernum = '".$row[1][race_pass_group]."' ";
$orders_query=mysql_query( $orders_sql_query, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$orows = fetch_all( $orders_query );
if( is_array( $orows[0] ))
{
 while($orow = each($orows) ) 
  {
$pdf->SetXY( 53 , 97 );
//$pdf->Cell(150 , 10 , $orow[1][inv_name], 0, 0, 'L', false);
$pdf->SetXY( 53 , 104 );
//$pdf->Cell(150 , 10 , $orow[1][inv_addr1], 0, 0, 'L', false);
$pdf->SetXY( 22 , 111 );
//$pdf->Cell(55 , 10 , $orow[1][inv_addr2], 0, 0, 'L', false);
$pdf->SetXY( 88 , 111 );
//$pdf->Cell(45 , 10 , $orow[1][inv_state], 0, 0, 'L', false);
$pdf->SetXY( 142 , 111 );
//$pdf->Cell(60 , 10 , $orow[1][inv_zip], 0, 0, 'L', false);
$pdf->SetXY( 43 , 118 );
//$pdf->Cell(85 , 10 , $orow[1][tel], 0, 0, 'L', false);
$pdf->SetXY( 148 , 118 );
//$pdf->Cell(55 , 10 , $orow[1][del_tel], 0, 0, 'L', false);
$pdf->SetXY( 21 , 125 );
//$pdf->Cell(55 , 10 , $orow[1][fax], 0, 0, 'L', false);
$pdf->SetXY( 43 , 132 );
//$pdf->Cell(160 , 10 , $orow[1][email], 0, 0, 'L', false);
$pdf->SetXY( 70 , 141 );
  }// end while orows
} //end if  
  
//$pdf->Cell(133 , 10 , $row[1][race_pass_adv_src], 0, 0, 'L', false);
$pdf->SetXY( 45 , 158 );
$pdf->Cell(158 , 10 , $row[1][race_pass_type], 0, 0, 'C', false);
$pdf->SetXY( 112 , 215 );
//$pdf->Cell(43 , 10 , $row[1][race_pass_paid], 0, 0, 'R', false);

$pdf->SetFont('BankGothicBT-Medium','',30);
$pdf->SetXY( 155 , 255 );
$pdf->Cell(48 , 10 , $row[1][id], 0, 0, 'C', false);
$pdf->SetFont('BankGothicBT-Medium','',8);
$pdf->SetXY( 14 , 232 );
//$pdf->MultiCell(140 , 3 , $row[1][race_pass_notes], 0, 'J', false);




$file_name = "signup_sheets/LARX_signup_sheet_".$row[1][id]."-".$row[1][driver_name]."-[".time()."]-".$row[1][race_pass_type].".pdf";
	 
$pdf->Output($file_name, 'F'); //saves as a file on the server
$pdf->Output($file_name, 'D'); //forces a download in the browser


 }  //end while 'race_passes' rows

mysql_close( $con );

?> 