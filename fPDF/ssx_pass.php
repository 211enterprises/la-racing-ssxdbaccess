<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2009-10-14  -  ssx_pass.php
#  script for "CREATING" a PDF for oaoer race passes to be delivered to customer.  Can be used
#  in automation or as the target for an html based web form.
#  Makes use of the fPDF package, pulls a PDF template file based on the data passed and template
#  file listed in the DB. Then simply adds customer specific data in the appropriate locations.
#  
# 2009-10-19 
#  This Script relies on a CONSTANT form base. Discovered today that Illustrator CS2 will change
#  the margin info of files created/last saved by CS3. Notably the top margin gets about 5pts 
#  added to it, shifting everything down, causing the text added here to appear shifted up.
#  So, the template files were saved as follows:
#   from CS3, save as PDF version 1.5 [Acrobat 6]. same options in CS2 tweak the form?!?
#  
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
// sending header info interferes with the PDF creating...
//session_start();
//if(empty($_SESSION['userId'])){
//    header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
//    echo "\$_SESSION['userId'] error";
//}
//echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";


require_once('fpdf.php'); 
require_once('fpdi.php'); 

include("config.php");
include("ssxDBfunctions.php");

$con = mysql_connect($DBhost,$DBuser,$DBpass) or die("Unable to connect to database $DBName");
$db  = mysql_select_db($DBName,$con) or die("Unable to select database $DBName");
$sql            ="select *                   from Admin           where       AdminName='$_POST[username]' and AdminPassword='$_POST[password]' ";
$sql_statement = "select race_pass_template from race_pass_types where race_pass_type ='$_POST[race_pass]' ";
$query=mysql_query( $sql_statement, $con )  or die('Could not Pull From DB:: ' . mysql_error());
$row = mysql_fetch_row( $query );
mysql_close( $con );

$nash_templ_vert_adj = 0;
if( $_POST[location] == 'Nashville' ){ $nash_templ_vert_adj -= 5; }



$pdf =& new FPDI(); 
$race_pass_template = 'blanks/'.$_POST[location].'/'.$row[0];

$pagecount = $pdf->setSourceFile( $race_pass_template ); 
$tplidx = $pdf->importPage(1, '/ArtBox'); 
 
$pdf->addPage(); 
$pdf->useTemplate($tplidx, 2, 1, 214); 
$pdf->AddFont('BankGothicBT-Medium','','bankgothicmedium.php');
$pdf->SetFont('BankGothicBT-Medium','',18);
$pdf->SetXY(126,54+$nash_templ_vert_adj);
$pdf->Cell(86 , 10 , $_POST['driver_name'], 0, 0, 'C', false);
$pdf->SetFont('BankGothicBT-Medium','',16);
$pdf->SetXY(126,65+$nash_templ_vert_adj);
if( $_POST['class_date'] == '0000-00-00' || $_POST['class_date'] == '9999-12-31' ){ $_POST['class_date'] = 'TBD'; }
$pdf->Cell(86 , 10 , $_POST['class_date'], 0, 0, 'C', false);

$pdf->SetXY(194,69+$nash_templ_vert_adj);
//$printed_time = date("g:i a", strtotime( $_POST['class_time'] )); # adds AM/PM to the time
$printed_time = date("g:i", strtotime( $_POST['class_time'] ));
$pdf->Cell(16 , 10 , $printed_time, 0, 0, 'C', false);

$pdf->SetXY(39,109+$nash_templ_vert_adj);
$pdf->Cell(65 , 10 , $_POST['expires'], 0, 0, 'C', false);
$pdf->SetXY(39,114+$nash_templ_vert_adj);
$pdf->Cell(65 , 10 , $_POST['pass_num'], 0, 0, 'C', false);

$file_name = "LARX Race Pass ".$_POST['pass_num']."-".$_POST[location].'-'.$_POST['driver_name']."-[".time()."]-".$_POST['race_pass'].".pdf";
//$file_name = "LARX Race Pass ".$_POST['pass_num']."-".$_POST['driver_name']."-[".time()."]-".$_POST['race_pass'].".pdf";
	 
$pdf->Output("race_passes/".$file_name, 'F'); //saves as a file on the server
$pdf->Output($file_name, 'D'); //forces a download in the browser

?> 