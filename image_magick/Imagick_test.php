<?
try
{
    /*** a new Imagick object ***/
    $im = new Imagick();

    /*** a new draw object ***/
    $draw = new ImagickDraw();

    /*** set the font ***/
    $draw->setFont('fonts/serpentinebold.ttf');

    /*** set the font size ***/
    $draw->setFontSize( 20 );

    /*** set the box color ***/
    $pixel = new ImagickPixel( 'red' );

    /*** the text to write ***/
    $text = 'Testee! Testee! Yep this script has what it takes, sir.';

    /*** get the font info ***/
    $font_info = $im->queryFontMetrics($draw, $text );

    /*** the width ***/
    $width = $font_info['textWidth'];

    /*** the height ***/
    $height = $font_info['textHeight'];

    /*** a new image with the dynamic sizes ***/
    $im->newImage($width+100, $height+100, $pixel);

    /*** set gravity to the center ***/
    $draw->setGravity( Imagick::GRAVITY_CENTER );

    /*** annotate the text on the image ***/
    $im->annotateImage($draw, 0, 20, 0, $text);

    /*** set the image format ***/
    $im->setImageFormat('png');

    /*** write image to disk ***/
    $im->writeImage( 'temp/text.png' );

    echo 'Image Created';
}
catch (Exception $e)
{
    echo $e->getMessage();
}

?>
<img src="temp/text.png">